/*
   Arduino auto dispenser
   fight corona!!!
   V1.0
   written by: Avihay Ashush
   Date: 25/03/2020
*/
#include <Servo.h>

//UltraSonic pins
#define TRIG 9
#define ECHO 10

//Servo pin
#define SERVO_PIN 3
#define START_POS 0
#define END_POS 110
#define SERVO_DELAY 500

//activation distance [cm]
#define MIN_DIST 15
//min time between actions in milli seconds
#define TIME_BETWEEN_ACTIONS 3000
//loop delay time in milli seconds
#define LOOP_DELAY 50

Servo disServo;
long signalTime, distance, actionTime;
bool actionFlag = true;

void setup() {
  pinMode(TRIG, OUTPUT);
  pinMode(ECHO, INPUT);
  disServo.attach(SERVO_PIN);
  disServo.write(START_POS);
}

void loop() {
  //measure distance
  getDist();
  //check proximity
  if (distance < MIN_DIST)
  {
    if (actionFlag)
    {
      activateDispenser();
      actionTime = millis();
      actionFlag = false;
    }
  }
  //check min time th
  if (!actionFlag)
  {
    if (millis() - actionTime > TIME_BETWEEN_ACTIONS)
      actionFlag = true;
  }
  delay(LOOP_DELAY);
}

/*
   getting distance from ultraSonic sensor
   using "outliar algo", sorting N meausments and taking only the middle one
*/
void getDist()
{
  long tempDist[5] = {0, 0, 0, 0, 0};
  int ArrSize = 5;
  for (int i = 0; i < ArrSize; i++)
  {
    digitalWrite(TRIG, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIG, LOW);
    signalTime = pulseIn(ECHO, HIGH); //measure passing time

    distance = (signalTime / 2) / 29.1; // Convert the time intoa distance
    if (distance == 0)
      i--;
    for (int k = 0; k <= i; k++)
      if (distance > tempDist[k])
      {
        for (int j = i; j > k; j--)
        {
          tempDist[j] = tempDist[j - 1];
        }
        tempDist[k] = distance;
        break;
      }
  }
  distance = tempDist[2];
}

// activate dispenser using servo
void activateDispenser()
{
  disServo.write(START_POS);
  delay(SERVO_DELAY);
  disServo.write(END_POS);
  delay(SERVO_DELAY);
  disServo.write(START_POS);
}
